{
    "use strict";
    //Module require
    const gulp 		= require("gulp"),
        sass 		= require("gulp-sass"),
        watch 		= require("gulp-watch"),
        babel 		= require("gulp-babel"),
        concat 		= require("gulp-concat"),
        uglify 		= require("gulp-uglify"),
        pug 		= require("gulp-pug"),
        imagemin 	= require('gulp-imagemin'),
        browserSync = require("browser-sync").create(),
        browserify 	= require("gulp-browserify");


    //Files path
    const scssPath 	= "public/src/scss/style.scss",
        allScssPath = "public/src/scss/**/*.scss",
        allJsPath	= "public/src/js/**/*.js",
        jsPath 		= "public/src/js/script.js",
        allImgPath	= "public/src/img/**/*.**",
        allViewPath = "views/**/*.pug",
        viewPath 	= "views/*.pug",
        bundlePath 	= "output/public/dist/";

    gulp.task("sass", () => {
        gulp.src(scssPath)
            .pipe(sass({outputStyle: "compressed"}))
            .pipe(gulp.dest(bundlePath + "css"))
            .pipe(browserSync.stream({match: '**/*.css'}));
    });

    gulp.task("img", () => {
        gulp.src(allImgPath)
            .pipe(imagemin())
            .pipe(gulp.dest(bundlePath + "img"));
    });

    gulp.task("js", () => {
        gulp.src(jsPath)
            .pipe(browserify())
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(concat("bundle.js"))
            .pipe(uglify())
            .pipe(gulp.dest(bundlePath + "js"));
    });

    gulp.task("view", () => {
        gulp.src(viewPath)
            .pipe(pug({
                pretty: true
            }))
            .pipe(gulp.dest("output"));
    });

    gulp.task("fonts", () => {
        gulp.src("public/src/fonts/**/**.**")
            .pipe(gulp.dest(bundlePath + "fonts"));
    });

    gulp.task("browser-sync", () => {
        browserSync.init({
            server: {
                baseDir: "./",
                index: "./output/index.html"
            }
        })
    });

    gulp.task("default", ["browser-sync", "sass", "js", "view", "img", "fonts"], () => {
        gulp.watch(allScssPath, ["sass"]);
        gulp.watch(allJsPath, ["js"]);
        gulp.watch(allViewPath, ["view"]);
        gulp.watch(allImgPath, ["img"]);

        gulp.watch(bundlePath + "**/*.js").on('change', browserSync.reload);
        gulp.watch("./output/*.html").on('change', browserSync.reload);
    });
}