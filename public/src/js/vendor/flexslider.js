require('flexslider');

function initSlider() {
    $('.flexslider').flexslider();
    $('.flexslider-slide').flexslider({
        animation: "slide",
        animationLoop: true,
        pauseOnHover: false
    });
    $('.flexslider-slide-3').flexslider({
        animation: "slide",
        animationLoop: true,
        pauseOnHover: false,
        itemWidth: $('.container').width()/3,
        minItems: $(window).width() > 768 ? 3 : 1,
        maxItems: $(window).width() > 768 ? 3 : 1
    });

    $('.vertical-slider').flexslider({
        animation: "slide",
        animationLoop: true,
        direction: "vertical",
        minItems: 2,
        maxItems: 2,
        itemMargin: 40,
        pauseOnHover: false
    });
}

module.exports = {
    init: initSlider
}