{
    // jquery
    let $ = global.$ || {};
    let jQuery = global.jQuery || {};

    $ = jQuery = require('jquery');

    global.$ = $;
    global.jQuery = jQuery;

    require('jquery-colorbox');
    require('jquery-validation');
    require('../../../node_modules/jquery-ui-slider/jquery-ui.js');
    require('jquery-datepicker');
    let Inputmask = require('inputmask');


    const sliders = require('./vendor/flexslider.js');

    $(function() {
        sliders.init();

        function bindHashControl() {
            if (window.location.hash) {
                if ($(window.location.hash).length > 0) {
                    $.colorbox({inline:true, href:window.location.hash, maxWidth:'95%', maxHeight:'95%', onClosed: function () {
                            window.history.replaceState({}, "", "#");
                        }});
                }
            }
        }
        // $('.datepicker').datepicker();
        Inputmask("05999999999",{ "clearIncomplete": true }).mask(document.querySelectorAll('.phone-mask'));
        Inputmask("09999999999",{ "clearIncomplete": true }).mask(document.querySelectorAll('.static-phone-mask'));
        Inputmask("99999999999",{ "clearIncomplete": true }).mask(document.querySelectorAll('.tc-mask'));
        Inputmask("99/99/9999",{ "clearIncomplete": true }).mask(document.querySelectorAll('.dt-mask'));
        Inputmask('decimal', { "clearIncomplete": true, 'autoGroup': true, rightAlign: false, 'groupSeparator': '.', 'radixPoint': ',', 'alias': 'decimal' }).mask(document.querySelectorAll('.custom-mask'));
        //static mask

        bindHashControl();
        window.onhashchange = function () {bindHashControl()};

        $(document).on('click', '.close-lightbox', function (e) {
            e.preventDefault();
            $.colorbox.close();
        });

        var money = function (num) {
            var num = num.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');

            return num.substr(0, num.length - 3);
        }

        $('[data-toggle]').on('click', function (e) {
            e.preventDefault();
            $(this).toggleClass('active');
            if ($(this).hasClass('active') && $(this).data('close-text')) {
                $(this).text($(this).data('close-text'))
            } else if ($(this).data('open-text')) {
                $(this).text($(this).data('open-text'))
            }
            $('[data-open="'+$(this).attr('data-toggle')+'"]').toggleClass('active');
        });

        $('[data-tab]').on('click', function (e) {
            e.preventDefault();
            $(this).addClass('active').siblings().removeClass('active');
            $('[data-open-tab="'+$(this).attr('data-tab')+'"]').toggleClass('active').siblings().removeClass('active');
        });

        $('.ikinci-el-filter .title').on('click', function (e) {
            e.preventDefault();
            $(this).parent().toggleClass('active');
        });

        /** FILTER START**/
        var $slider = $(".slider"),
            $filterItem = $('.filter-item'),
            $markaCheckboxs = $('[name="marka"]'),
            $modelCheckboxs = $('[name="model"]');
        $slider.slider({
            range: true,
            min: 50000,
            max: 1000000,
            step: 5000,
            values: $slider.data('selected') ? $slider.data('selected') : [50000, 1000000],
            create: function (event, ui) {
                var values = $slider.slider("option", "values"),
                    $amount = $slider.next('.amount');
                $amount.find('.min span').text(money(values[0]) + " TL ");
                $amount.find('.max span').text(money(values[1]) + " TL");
            },
            slide: function (event, ui) {
                var $amount = $slider.next('.amount');
                $amount.find('.min span').text(money(ui.values[0]) + " TL ");
                $amount.find('.max span').text(money(ui.values[1]) + " TL");
            }
        });

        $filterItem.find('.input input').on('keyup', function () {
            var $this = $(this),
                $listArea = $this.parent().next('.search-list').find('label');

            if ($this.val().length === 0) {
                $listArea.removeClass('hidden');
            } else {
                $listArea.each(function () {
                    if ($(this).attr('val').toLowerCase().indexOf($this.val().toLowerCase()) === -1) {
                        $(this).addClass('hidden');
                    } else {
                        $(this).removeClass('hidden');
                    }
                });
            }
        });

        $markaCheckboxs.on('change', function () {
            checkSelectedModels();
        });

        function checkSelectedModels() {
            $modelCheckboxs.parents('label').addClass('hidden');
            $markaCheckboxs.each(function () {
                var $this = $(this),
                    isSelected = $this.is(':checked');

                if (isSelected) {
                    $modelCheckboxs.each(function () {
                        var $label = $(this).parents('label');
                        if ($label.attr('marka').toLowerCase().indexOf($this.val().toLowerCase()) !== -1) {
                            $label.removeClass('hidden');
                        }
                    });
                }
            });
        }

        function letsSearch(pathname) {
            var url = pathname + '?';
            $filterItem.each(function () {
                var $this = $(this),
                    $selectedItems = $this.find('input[type="checkbox"]').length > 0 ? $this.find('input[type="checkbox"]:checked') : $this.find('input[type="radio"]:checked');
                if ($selectedItems.length > 0) {
                    url+= $selectedItems.attr('name') + '=[';
                    $selectedItems.each(function () {
                        url += $(this).val() + ',';
                    });
                    url = url.substr(0, url.length -1);
                    url+= ']&'
                } else if ($this.find('input[type="text"]')) {
                    $this.find('input[type="text"]').each(function () {
                        if ($(this).val())
                            url += '&' + $(this).attr('name') +'=' + $(this).val();
                    });
                }
            });
            if ($slider && $slider.length > 0) {
                var prices = $slider.slider("option", "values");

                url += 'min-tutar=' + prices[0] + '&max-tutar=' + prices[1];
            }
            window.location.href = url;
        }

        //sonuçları görüntüle click eventi
        $('#lets-search').on('click', function () {
            letsSearch($(this).data('href') || window.location.pathname);
        });

        //temizle click eventi
        $('#reset-search').on('click', function () {
            window.location.href = window.location.pathname
        });

        /** FILTER END**/


        //Anasayfa autocomplete

        // Search js
        $('.search-box input').on('focus', function () {
            $(this).parents('.search-box').addClass('focus');
            $('body').css('overflow', 'hidden');
        }).on('blur', function () {
            var $this = $(this);
            setTimeout(function () {
                $this.parents('.search-box').removeClass('focus');
                $('body').css('overflow', 'auto');
            }, 300);
        });

        var $searchInput = $('#searchInput'),
            timeOut;
        $searchInput.on('keyup', function (evt) {
            var key = $searchInput.val();
            if (evt.keyCode != 38 && evt.keyCode != 40)
            if (key.length > 2) {
                $('.autocomplete-list').addClass('hidden').empty();
                clearInterval(timeOut);
                timeOut = setTimeout(function () {
                    $.get(root + '/search/auto-complete/' + key + '/', function (response) {
                        for (var i = 0; i < 4; i++) {
                            if (response["product"] && response["product"].length > 0) {
                                $('.autocomplete-list').append('<li><a href="'+root+'/arac/'+response["product"][i]['slug'] +'">'+response["product"][i]['name']+'</a></li>').removeClass('hidden')
                            } else {
                                $('.autocomplete-list').addClass('hidden').empty();
                            }
                        }
                    });
                },300)
            }
        });

        $(document).on('keyup',function(evt) {
            var $autocomplete = $('.autocomplete-list'),
                $activeItem = $autocomplete.find('.active');
            if ($autocomplete.is(':visible')) {
                //up
                if (evt.keyCode == 38) {
                    if ($activeItem.length > 0) {
                        $activeItem.removeClass('active').prev('li').addClass('active');
                    } else {
                        $autocomplete.find('li:last-child').addClass('active');
                    }
                } else if (evt.keyCode == 40) {
                    if ($activeItem.length > 0) {
                        $activeItem.removeClass('active').next('li').addClass('active').find('a').trigger('focus');
                    } else {
                        $autocomplete.find('li:first-child').addClass('active');
                    }
                }
            }
        });


        //form validations
        $('.validate-form').each(function () {
            var $this = $(this),
                url = $this.attr('action'),
                redirect = $this.attr('redirect');
            $(this).validate({
                errorPlacement: function(error, element) {
                    // Append error within linked label
                    $( element )
                        .parent().addClass('error');
                },
                submitHandler: function(form) {
                    $.get(root + url, $this.serializeArray(), function (res) {
                        if (res.success)
                            if (redirect) {
                                window.location.href=redirect;
                            } else {
                                window.location.reload();
                            }
                        else {
                            $('#form-alert p').text(res['message']);
                            window.location.href="#form-alert"
                        }
                    })
                }

            })
        });

        //Talep oluşturma formu

        // $('.send-request').on('click', function (e) {
        //     e.preventDefault();
        //     var $this = $(this),
        //         selectedSlug = $('[name="slug"]:checked').val();
        //     if (selectedSlug) {
        //         window.location.href = $(this).attr('href');
        //     } else {
        //         $('#form-alert p').text('Lütfen bir opsiyon seçiniz..');
        //         window.location.href="#form-alert"
        //     }
        // });
        $('.request-form').on('submit', function (e) {
            e.preventDefault();
            var data = $(this).serializeArray();
            data['slug'] = $('[name="slug"]:checked').val();
            $.post(root + '/basvuru/talep-olustur/', data, function (response) {
               if (response['success']) {
                   window.location.href = "#istek-basarili"
               } else {
                   $('#form-alert p').text(response['message']);
                   window.location.href = "#form-alert"
               }
            });
        });

        //talep sil
        $('.request-options-delete').on('click', function (e) {
            e.preventDefault();
            var $this = $(this);
            var r = confirm('Talebi silmek istediğinize emin misiniz?');

            if (r == true) {
                $.get(root + '/basvuru/talep-sil/' + $(this).data('key') + '/', null, function (res) {
                    if(res['status'] == "success") {
                        window.location.reload();
                    } else {
                        alert(res['message']);
                    }
                });
            }
        });

        $('.flex-list, .flex-list .container').css('min-height', $(window).height()-100);

        var $questionBlock = $('.faq-list .question');
        $questionBlock.on('click', function () {
            $questionBlock.removeClass('active');
            $('.faq-list .answer').not($(this).next('.answer')).slideUp();
            $(this).addClass('active').next('.answer').slideToggle();
        });

        //settings
        $('.settings-list .edit-setting').on('click', function (e) {
            e.preventDefault();
            $(this).parents('li').find('.front').hide();
            $(this).parents('li').find('.backend').show();
        })
        $('.settings-list .close-setting').on('click', function (e) {
            e.preventDefault();
            $(this).parents('li').find('.front').show();
            $(this).parents('li').find('.backend').hide();
        })

    });

    $('.select-filter').on('change', function () {
       var value = $(this).val();
       var param = $(this).data('param');

        var queryParams = new URLSearchParams(window.location.search);


        if (value === "" || !value)
            queryParams.delete(param);
        else
            queryParams.set(param, value);

        window.location.href= "?"+queryParams.toString();
    });
}